#!/bin/bash

set -e

if [ "$1" = 'init' ]; then
    if [ ! -d /var/lib/postgresql/data ]; then
	    pg_ctl initdb -D /var/lib/postgresql/data
		cat /etc/postgresql/init.sql | postgres --single -D /etc/postgresql postgres
		exec echo "Database created."
    fi
	chown -Rf postgres:postgres /var/lib/postgresql/data
    chmod -R 700 /var/lib/postgresql/data
    cat /etc/postgresql/update.sql | postgres --single -D /etc/postgresql postgres
    exec echo "Initialization complete."
fi

if [ "$1" = 'postgresql' ]; then
    exec postgres -D /etc/postgresql
fi

exec "$@"
