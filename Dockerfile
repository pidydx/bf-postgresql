#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=postgres
ENV APP_GROUP=postgres

# Set version pins
ENV POSTGRESQL_VERSION=14

ENV BASE_PKGS postgresql-${POSTGRESQL_VERSION} postgresql-contrib-${POSTGRESQL_VERSION}

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -d /var/lib/${APP_USER} -s /usr/sbin/nologin

# Update and install base packages
COPY etc/apt /etc/apt

RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*

##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY usr/ /usr/

RUN rm -Rf /etc/postgresql/${POSTGRESQL_VERSION}

ENV PATH /usr/lib/postgresql/${POSTGRESQL_VERSION}/bin/:$PATH
EXPOSE 5432/tcp
VOLUME ["/etc/postgresql", "/var/lib/postgresql"]

USER ${APP_USER}
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["postgresql"]
